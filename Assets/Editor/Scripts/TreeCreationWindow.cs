﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;
using DatumApps;
class TreeCreationWindow : EditorWindow
{
	[MenuItem ("Window/Low Poly Enviroment")]
	public static void  ShowWindow ()
	{
        EditorWindow window = EditorWindow.GetWindow (typeof(TreeCreationWindow));
        window.title = "Low Poly Generator";

	}

	string treeName = "Tree";
	bool showAdvancedOptions = false;
	bool shiftOnPlace = true;
	bool embedMeshColors = false;
    bool showTrees = true;

    bool showRocks = true;
    TreeMaker maker = new TreeMaker();
    RockMaker rockMaker = new RockMaker();

	void OnGUI ()
	{
        showTrees = EditorGUILayout.BeginToggleGroup ("Draw Trees", showTrees);
        if(showTrees){
            DrawTreeOptions();
        }
        EditorGUILayout.EndToggleGroup ();

        showRocks = EditorGUILayout.BeginToggleGroup ("Draw Rocks", showRocks);
        if(showRocks){
            DrawRockOptions();
        }
        EditorGUILayout.EndToggleGroup ();

	}


    private void DrawTreeOptions(){
        
        treeName = EditorGUILayout.TextField ("Name", treeName);
        maker.origin = EditorGUILayout.Vector3Field ("Origin", maker.origin);
        shiftOnPlace = EditorGUILayout.Toggle("Randomize Origin",shiftOnPlace);
        maker.branchColor = EditorGUILayout.ColorField("Leaf Color",maker.branchColor);
        maker.trunkColor = EditorGUILayout.ColorField("Trunk Color",maker.trunkColor);
        maker.totalHeight = EditorGUILayout.FloatField("Tree Height",maker.totalHeight);
        
        EditorGUILayout.LabelField("Tree Type");
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button ("Poplar")) {
            maker.setDefaults(TreeMaker.TreePreset.Poplar);
        }
        else if (GUILayout.Button ("Pine")) {
            maker.setDefaults(TreeMaker.TreePreset.Pine);
        }
        else if (GUILayout.Button ("Oak")) {
            maker.setDefaults(TreeMaker.TreePreset.Oak);
        }
        EditorGUILayout.EndHorizontal();
        showAdvancedOptions = EditorGUILayout.BeginToggleGroup ("Advanced Settings", showAdvancedOptions);
        if(showAdvancedOptions){
            embedMeshColors = EditorGUILayout.Toggle("Use Mesh Colors",embedMeshColors);
            maker.topRadius = EditorGUILayout.FloatField("Top Radius",maker.topRadius);
            maker.bottomRadius = EditorGUILayout.FloatField("Bottom Radius",maker.bottomRadius);
            maker.treeHeight = EditorGUILayout.FloatField("Branch Height",maker.treeHeight);
            maker.trunkHeight = EditorGUILayout.FloatField("Trunk Height",maker.trunkHeight);
            maker.pointCount = EditorGUILayout.IntField("Number of Points",maker.pointCount);
            maker.trunkPointCount = EditorGUILayout.IntField("Number of Points on Trunk",maker.trunkPointCount);
            
            maker.pointyTop = EditorGUILayout.Toggle("Pointy Top",maker.pointyTop);
            maker.vertexNoise = EditorGUILayout.Toggle("Apply Noise",maker.vertexNoise);
        }
        EditorGUILayout.EndToggleGroup ();
        
        if (GUILayout.Button ("Grow Tree")) {
            MakeTree ();
        }
    }

    private void DrawRockOptions(){
        rockMaker.boundingBox = EditorGUILayout.BoundsField("Rock Bounding Box",rockMaker.boundingBox);
        rockMaker.minStep = EditorGUILayout.FloatField("Minimum Step",rockMaker.minStep);
        rockMaker.maxStep = EditorGUILayout.FloatField("Maximum Step",rockMaker.maxStep);
        rockMaker.pointsPerRing = EditorGUILayout.IntField("Points Per Step",rockMaker.pointsPerRing);

    }



	private void MakeTree ()
	{
		maker.MakeTree(treeName,shiftOnPlace,embedMeshColors);
	}



}