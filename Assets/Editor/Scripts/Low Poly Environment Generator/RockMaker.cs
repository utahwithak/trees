﻿using UnityEngine;
using System.Collections;
namespace DatumApps{

    public class RockMaker  {
        public Bounds boundingBox = new Bounds(Vector3.zero,Vector3.one);
        public float maxStep = 1;
        public float minStep = 0.1f;
        public int pointsPerRing = 10;
    }
}
