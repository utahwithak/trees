﻿using UnityEngine;
using UnityEditor;

using System.Collections.Generic;
namespace DatumApps{
public class TreeMaker{

		static int[] SimpleTri = {0,1,2};
		public enum TreePreset{
			Poplar,
			Oak,
			Pine
		}
		public TreeMaker(){
			setDefaults(TreePreset.Oak);
		}
		
		public float topRadius = 2;
		public float bottomRadius = 1;
		public Color32 branchColor = new Color32(37,131,49,255);
		public Color32 trunkColor = new Color32(182,151,106,255);
		public float treeHeight = 2.8f;
		public float trunkHeight = .2f;
		
		public float totalHeight = 3;
		public int pointCount = 16;
		public int trunkPointCount = 16;
		public bool pointyTop = false;
		public bool vertexNoise = false;
		public Vector3 origin = Vector3.zero;
		int count = 0;
		public GameObject MakeTree(string name, bool shiftOnPlace, bool embedColors){
			int operation = Undo.GetCurrentGroup();
			
			GameObject newTree = new GameObject(name+ (count++ > 0? (count).ToString():"")) ;
			Undo.RegisterCreatedObjectUndo (newTree, "Created Tree");
			
			List<Point3d> lastRing = DrawTrunk(newTree,embedColors);
			
			List<Point3d> treePoint = new List<Point3d>(lastRing);
			
			float rStep = (topRadius - bottomRadius) / treeHeight;
			
			for (float h = 0; h < treeHeight; h += treeHeight/10.0f ){
				float radius = ((h * rStep) + bottomRadius);
				for(int i = 0; i < pointCount; i ++){
					if(pointyTop){
						Vector3 rand = UnityEngine.Random.insideUnitCircle.normalized * radius;
						treePoint.Add( new Point3d(rand.x , trunkHeight + h , rand.y ));
					}
					else{
						//rounded
						Vector3 curRand = UnityEngine.Random.onUnitSphere * radius;
						curRand = normalizeVector(curRand,i);
						float val = Mathf.Max(curRand.y + trunkHeight  + h, 0);
						treePoint.Add( new Point3d(curRand.x ,  val , curRand.z ));
					}
				}
			}
			
			if(pointyTop){
				treePoint.Add( new Point3d(0 , trunkHeight + treeHeight + (topRadius * 1.01) ,0 ));
			}
			
			
			GameObject go = new GameObject("Tree");
			Undo.RegisterCreatedObjectUndo (go, "Created Tree");
			MeshRenderer treeRenderer = go.AddComponent<MeshRenderer>();
			Material treeMat = new Material(Shader.Find("Diffuse"));
			treeMat.color = branchColor;
			treeRenderer.material = treeMat;
			MeshFilter treeFilter = go.AddComponent<MeshFilter>();
			Color32? treeCol = null;
			if(embedColors){
				treeCol = branchColor;
			}
			treeFilter.mesh = MeshFromPoints(treePoint,vertexNoise,treeCol);

			
			go.transform.parent = newTree.transform;
			newTree.transform.position = origin;
			Selection.activeGameObject = newTree;
			SceneView.lastActiveSceneView.FrameSelected();

			
			if(shiftOnPlace){
				Vector2 offset = UnityEngine.Random.insideUnitCircle.normalized * (bottomRadius * UnityEngine.Random.Range(2f,3f));
				origin += new Vector3(offset.x,0,offset.y);
			}

			Undo.CollapseUndoOperations(operation);
			return newTree;
		}

		public void setDefaults(TreePreset type){
			switch(type){
			case TreePreset.Pine:
				bottomRadius = totalHeight * UnityEngine.Random.Range(.1f,.5f) ;
				topRadius = .01f;
				pointyTop = true;
				trunkHeight = totalHeight * .1f;
				treeHeight = totalHeight * .9f;
				pointCount = 16;
				break;
			case TreePreset.Poplar:
				bottomRadius = totalHeight * UnityEngine.Random.Range(.3f,.8f) ;
				topRadius = bottomRadius * UnityEngine.Random.Range(.3f,.8f);
				pointyTop = false;
				trunkHeight = bottomRadius + totalHeight * .1f;
				treeHeight = totalHeight * .9f;
				pointCount = 20;
				break;
			case TreePreset.Oak:
				bottomRadius = totalHeight * UnityEngine.Random.Range(.8f,3f) ;
				topRadius = bottomRadius * UnityEngine.Random.Range(.8f,.9f);
				pointyTop = false;
				trunkHeight = bottomRadius + totalHeight * .3f;
				treeHeight = .2f;
				pointCount = 32;
				break;
				
			}
		}

		private List<Point3d> DrawTrunk(GameObject parent, bool embedColors){
			Color32? tColor = null;
			if(embedColors){
				tColor = trunkColor;
			}
			Material trunkMat = new Material(Shader.Find("Diffuse"));
			trunkMat.color = trunkColor;
			List<Point3d> lastRing = new List<Point3d>();
			float ringRadius = UnityEngine.Random.Range(bottomRadius * .1f, bottomRadius * .2f);
			
			for(float ringY = 0; ringY < trunkHeight ; ringY += trunkHeight/3.0f){
				
				ringRadius = UnityEngine.Random.Range(ringRadius * .66f,ringRadius );
				List<Point3d> curRing = new List<Point3d>();
				
				for(int i = 0; i < trunkPointCount ; i++){
					Vector3 curRand = UnityEngine.Random.insideUnitCircle.normalized * ringRadius;
					curRand = normalizeVector(curRand,i);
					curRing.Add(new Point3d(curRand.x,ringY, curRand.y));
				}
				
				if(lastRing.Count != 0){
					List<Point3d> trunkSection = new List<Point3d>(curRing);
					trunkSection.AddRange(lastRing);
					
					
					GameObject trunk = new GameObject("TrunkSection:"+ringY);
					Undo.RegisterCreatedObjectUndo (trunk, "Created trunk");
					
					trunk.transform.parent = parent.transform;
					MeshRenderer renderer = trunk.AddComponent<MeshRenderer>();
					renderer.material = trunkMat;
					MeshFilter filter = trunk.AddComponent<MeshFilter>();

					Mesh newMesh = MeshFromPoints(trunkSection,false,tColor);
					
					filter.mesh = newMesh;
					
				}
				lastRing = curRing;
			}
			return lastRing;
		}
		


		
		public static Vector3 normalizeVector(Vector3 curRand, int i){
			switch((i%8)){
			case 0:
				curRand.x = 1 * Mathf.Abs(curRand.x);
				curRand.y = 1 * Mathf.Abs(curRand.y);
				curRand.z = 1 * Mathf.Abs(curRand.z);
				break;
			case 1:
				curRand.x = -1 * Mathf.Abs(curRand.x);
				curRand.y = -1 * Mathf.Abs(curRand.y);
				curRand.z = -1 * Mathf.Abs(curRand.z);
				break;
			case 2:
				curRand.x = -1 * Mathf.Abs(curRand.x);
				curRand.y = 1 * Mathf.Abs(curRand.y);
				curRand.z = 1 * Mathf.Abs(curRand.z);
				break;
			case 3:
				curRand.x =  1 * Mathf.Abs(curRand.x);
				curRand.y = -1 * Mathf.Abs(curRand.y);
				curRand.z =  1 * Mathf.Abs(curRand.z);
				break;
			case 4:
				curRand.x =  1 * Mathf.Abs(curRand.x);
				curRand.y =  1 * Mathf.Abs(curRand.y);
				curRand.z = -1 * Mathf.Abs(curRand.z);
				break;
			case 5:
				curRand.x = -1 * Mathf.Abs(curRand.x);
				curRand.y = -1 * Mathf.Abs(curRand.y);
				curRand.z =  1 * Mathf.Abs(curRand.z);
				break;		  
			case 6:
				curRand.x =  1 * Mathf.Abs(curRand.x);
				curRand.y = -1 * Mathf.Abs(curRand.y);
				curRand.z = -1 * Mathf.Abs(curRand.z);
				break;
			case 7:
				curRand.x = -1 * Mathf.Abs(curRand.x);
				curRand.y =  1 * Mathf.Abs(curRand.y);
				curRand.z = -1 * Mathf.Abs(curRand.z);
				break;
			}
			return curRand;
			
		}
		


		private static Mesh MeshFromPoints(List<Point3d> points, bool randomize = false, Color32? vertColor = null){
			
			QuickHull3D hull = new QuickHull3D ();
			hull.build (points.ToArray());
			
			Point3d[] vertices = hull.getVertices ();
			
			int[][] faceIndices = hull.getFaces ();
			for (int i = 0; i < faceIndices.Length; i++) {
				string f = "";
				for (int k = 0; k < faceIndices[i].Length; k++) {
					f += (faceIndices [i] [k] + " ");
				}
			}
			
			List<Vector3> verts = new List<Vector3>();
			List<int> tris = new List<int>();
			Dictionary<int,List<int>> vertMap = new Dictionary<int, List<int>>();
			List<Color32> meshColors  = new List<Color32>();

			for (int i = 0; i < faceIndices.Length; i++) {
				int[] curTris = SimpleTri;
				
				if(faceIndices[i].Length != 3){
					List<Vector2> faceVerts = new List<Vector2>();
					for (int k = 0; k < faceIndices[i].Length; k++) {
						int vIndex = faceIndices[i][k];
						Point3d pnt = vertices [vIndex];
						Vector2 cur = new Vector2((float)pnt.x,(float)pnt.z);
						faceVerts.Add(cur);
						
					}
					Triangulator t = new Triangulator(faceVerts.ToArray());
					curTris = t.Triangulate();
					
				}
				foreach(int k in curTris) {
					int vIndex = faceIndices[i][k];
					
					if(!vertMap.ContainsKey(vIndex)){
						vertMap[vIndex] = new List<int>();
					}
					Point3d pnt = vertices [vIndex];
					
					Vector3 cur = new Vector3((float)pnt.x,(float)pnt.y,(float)pnt.z);
					vertMap[vIndex].Add(verts.Count);
					verts.Add(cur);
					tris.Add(tris.Count);
					if(vertColor.HasValue){
						meshColors.Add(vertColor.Value);
					}
				}
				
			}
			
			Mesh newMesh = new Mesh();
			newMesh.vertices = verts.ToArray();
			newMesh.triangles = tris.ToArray();
			if(meshColors.Count != 0){
				newMesh.colors32 = meshColors.ToArray();
			}
			newMesh.uv = new Vector2[verts.Count];
			newMesh.RecalculateBounds();
			newMesh.RecalculateNormals();
			if(randomize){
				//Apply random noise to all verts
				Vector3[] normals = newMesh.normals;
				for(int i = 0; i < vertices.Length; i++){
					Vector3 avg =  Vector3.zero;
					foreach(int index in vertMap[i]){
						avg += normals[index];
					}
					avg =  (avg/(float)(vertMap[i].Count)).normalized * UnityEngine.Random.Range(-1,1);
					
					foreach(int index in vertMap[i]){
						verts[index] = verts[index] + avg; 
					}
					
				}
				newMesh.vertices = verts.ToArray();
				newMesh.RecalculateBounds();
				newMesh.RecalculateNormals();
			}
			return newMesh;
		}
		

}
}